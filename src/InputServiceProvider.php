<?php

namespace appnic\inputsforblade;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class InputServiceProvider extends ServiceProvider {
    private static $CONFIG_PATH =  __DIR__.'/../config';

    public function register() {
        $this->mergeConfigFrom(self::$CONFIG_PATH.'/default.php', 'inputs');
    }

    public function boot() {
        Processor::registerFields();
        Processor::registerAliases();

        $this->publishes([
            self::$CONFIG_PATH.'/inputs.php' => config_path('inputs.php')
        ], 'appnic-inputs-config');
    }
}