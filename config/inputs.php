<?php
/*
 * Check out the readme file (README.md) to see an explanation and
 * more information for each configuration element
 */
return [
    /*
     * Define the CSS classes for each element here
     */
    'classes' => [
        'input' => '',
        'label' => '',
        'select' => '',
        'option' => '',
        'form' => '',
        'button' => '',
        'checkbox' => '',
        'number' => '',
        'email' => '',
        'password' => '',
        'submit' => '',
    ],

    /*
     * This is the CSS base class which will be added to every element.
     */
    'baseclass' => '',

    /*
     * Here you can define additional fields or overwrite existing ones.
     * Check out the default config file (/vendor/appnic/inputs-for-blade/config/default.php)
     * and the readme file to see all the possibilities.
     */
    'fields' => [
        //---------------------------
        // Example field definition
        //---------------------------
        //
        // 'input' => [
        //     'html' => '<input name="{name}" type="{type}" class="{class}" value="{value}" />',
        //     'attributes' => [ 'name', 'value', 'type', 'class' ],
        //     'defaults' => [
        //         'type' => 'text'
        //     ]
        // ],
    ],

    /*
     * Define additional aliases below.
     */
    'aliases' => [
        //--------------------------
        // Example alias definition
        //--------------------------
        //
        // 'checkbox' => [
        //     'key'=>'input',
        //     'values'=>[ 'type'=>'checkbox' ]
        // ],
    ],
];