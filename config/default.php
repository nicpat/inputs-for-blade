<?php

return [
    'builtin_fields' => [
        'input' => [
            'html' => '<input name="{name}" type="{type}" class="{class}" value="{value}" />',
            'attributes' => [ 'name', 'value', 'type', 'class' ],
            'defaults' => [
                'type' => 'text'
            ]
        ],
        'label' => [
            'html' => '<label for="{for}" class="{class}">{text}</label>',
            'attributes' => [ 'text', 'for', 'class' ],
            'defaults' => []
        ],
        'select' => [
            'html' => '<select name="{name}" class="{class}">',
            'attributes' => [ 'name', 'class' ],
            'defaults' => []
        ],
        'option' => [
            'html' => '<option value="{value}">{text}</option>',
            'attributes' => [ 'value', 'text' ],
            'defaults' => []
        ],
        'endselect' => [
            'html' => '</select>',
            'attributes' => [],
            'defaults' => []
        ],
        'form' => [
            'html' => '<form action="{action}" class="{class}" method="POST"><input type="hidden" name="_method" value="{method}">',
            'attributes' => [ 'action', 'method', 'class' ],
            'defaults' => [
                'method' => 'POST'
            ]
        ],
        'endform' => [
            'html' => '</form>',
            'attributes' => []
        ],
        'button' => [
            'html' => '<button name="{name}" type="{type}" class="{class}">{text}</button>',
            'attributes' => [ 'name', 'text', 'type', 'class' ],
            'defaults' => [
                'type' => 'button'
            ]
        ]
    ],
    'builtin_aliases' => [
        'checkbox' => [
            'key'=>'input',
            'values'=>[ 'type'=>'checkbox' ]
        ],
        'number' => [
            'key'=>'input',
            'values'=>[ 'type'=>'number' ]
        ],
        'email' => [
            'key'=>'input',
            'values'=>[ 'type'=>'email' ]
        ],
        'password' => [
            'key'=>'input',
            'values'=>[ 'type'=>'password' ]
        ],
        'submit' => [
            'key'=>'button',
            'values'=>[ 'type'=>'submit' ]
        ]
    ],
    'fields' => [],
    'aliases' => [],
    'baseclass' => '',
    'classes' => [
        'input' => '',
        'label' => '',
        'select' => '',
        'option' => '',
        'form' => '',
        'button' => '',
        'checkbox' => '',
        'number' => '',
        'email' => '',
        'password' => '',
        'submit' => '',
    ]
];